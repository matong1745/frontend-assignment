import Vue from 'vue'
import Vuex from 'vuex'
// import createPersistedState from 'vuex-persistedstate'

import loading from './loading'

Vue.use(Vuex)

// const persistedstate = new createPersistedState({
//     key: 'key-name-on-local-storage',
//     paths: [ 'user' ]
// })

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    loading,
  },
// plugins: [ persistedstate ]
})
